# Traduire un projet en équipe avec OmegaT

OmegaT permet de traiter un projet de traduction à plusieurs quasiment en temps réel via l'internet.
Le projet est conservé sur un serveur et toutes vos contributions (traductions de segments, ajouts d'entrées de glossaire, etc.) y sont centralisées de manière à ce que tous les membres de l'équipe, dont vous-même, en bénéficient.

Notez que une fois que vous avez récupéré le projet OmegaT sur votre ordinateur, vous pouvez travailler dessus que vous soyez ou non connecté·e à l'internet. Si vous n'êtes pas connecté lors de votre session de travail dans OmegaT, vos contributions et celles des autres membres ne sont pas synchronisées en temps réel&nbsp;; la synchronisation se fera automatiquement une fois que vous êtes connecté·e à l'internet.

## Procédure

La traduction d'un projet en équipe comporte deux étapes&nbsp;:

 1. Vous devez télécharger le projet OmegaT (et cela fait, vous pouvez commencer à le traduire, réviser ou relire).
 2. Si après le téléchargement (et vos premières actions éventuelles sur les fichiers à traduire), vous fermez le projet en équipe (et quittez ou pas OmegaT), vous **devez** rouvrir le projet comme tout projet ordinaire.

### 1. Télécharger le projet en équipe

1. Dans OmegaT, *Projet* > *Télécharger projet en équipe*.
2. Dans la boîte de dialogue qui s'affiche, saisissez l'*URL du dépôt* que le ou la chef·fe de projet vous aura communiqué par courriel. OmegaT vous propose en outre de créer un dossier local (sur votre ordinateur) pour le projet et vous propose automatiquement un emplacement pour le projet local. Confirmez (OK)&nbsp;: le projet sera téléchargé et enregistré à l'emplacement spécifié et vous pouvez commencer à traduire (ou réviser ou relire) les fichiers sources du projet.

Par défaut, le projet est synchronisé toutes les 3 minutes&nbsp;: tout segment que vous aurez traduit (ou révisé ou relu et modifié) et toute entrée de glossaire que vous aurez ajoutée (ou modifiée) sera disponible pour les autres membres du projet en équipe (et vice versa).

### 2. Ouvrir un projet en équipe déjà téléchargé

Un projet en équipe ne doit être téléchargé qu'une et une seule fois. Si vous voulez ensuite retravailler sur le projet (c.-à-d. rouvrir le projet dans OmegaT après que vous l'avez fermé), vous devez utiliser la commande ordinaire d'ouverture de projet (*Projet* > *Ouvrir* ou *Ouvrir un projet récent*).

**Notes**

Vous pouvez réduire ou augmenter l'intervalle de synchronisation via *Options* > *Préférences* > *Enregistrement et exécution* (options *Minutes* et *Secondes*).